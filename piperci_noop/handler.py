from flask import Request
from typing import Dict, Tuple, Union

from piperci.task.exceptions import PiperDelegateError, PiperError
from piperci.task.this_task import ThisTask


def executor(
    request: Request, task: ThisTask, config: dict
) -> Tuple[Union[Dict, str], int]:
    """
    Entrypoint to the noop task async executor function.
    :param request: The request object from Flask
    :param task: The ThisTask instance for this class required for this task
    :param config:
    :return: (response_body, code)
    """
    task.info(str(task.task))
    # See piperci.task.this_task for gman.task interface usage docs
    try:
        task.info("Noop task gateway handler.executor called successfully")

        # ## Replace below path with desired upload path
        execute_code(request, task, config)

        task.artifact(config["local_artifact_path"])
        task.complete("Noop task complete")
        return {"test": "test"}, 200
    except PiperError:
        return "", 400


def execute_code(request: Request, task: ThisTask, config: dict):
    # Replace below with task execution logic
    if config["task_config"]["config"]["with_artifact"]:
        artifact = f"noop artifact content {task.task['task']['task_id']}"
        task.info("with_artifact == True, Writing artifact")
        with open(config["local_artifact_path"], "w") as artfile:
            artfile.write(artifact)
    else:
        task.info("with_artifact == False, Skipping artifact")


def gateway(request: Request, task: ThisTask, config: dict):
    """
    Entrypoint to the noop Task.
    :param request: The request object from Flask
    :param task: The ThisTask instance for this class required for this task
    :return: (response_body, code)
    """

    try:
        task.info("Noop task gateway handler.handle called successfully")
    except PiperError as e:
        return {"error": f"{str(e)}: no delegate was attempted"}, 400
    data = config["task_config"]
    try:
        task.delegate(config["executor_url"], data)
        return task.complete("Noop gateway completed successfully")
    except PiperDelegateError as e:
        return {"error": f"{str(e)}"}, 400
