#!/bin/bash

THIS_SCRIPT=$(basename "${BASH_SOURCE[0]}")
THIS_RELDIR=$(dirname "${BASH_SOURCE[0]}")
REPO_BASEDIR=$(cd $THIS_RELDIR; cd ../../; pwd)

usage() {
  echo "usage: $0 [GIT_REPO_URL] [GIT_BRANCH] [DEST_TEMPLATE_PATH]"
  echo "usage: $0 [TAR_DOWNLOAD_URL] [DEST_TEMPLATE_PATH]"
  echo "usage: $0 -h"
}

if [[ "$*" =~ .*-h|--help.* ]];
then
  usage
  exit 0
fi

if [[ ${#@} -eq 3 ]]; then
  GIT_REPO_URL=$1
  GIT_BRANCH=$2
  TEMPLATE_INSTALL_BASE_PATH=$3
elif  [[ ${#@} -eq 2 ]]; then
  CURL_DOWNLOAD_URL=$1
  TEMPLATE_INSTALL_BASE_PATH=$2
else
  echo "Incorrect number of params"
  usage
  exit 1
fi

cd "${REPO_BASEDIR}" || echo "${REPO_BASEDIR} does not exist, exiting" exit 1

if ! [[ -d ${TEMPLATE_INSTALL_BASE_PATH} ]];
then
  echo "${TEMPLATE_INSTALL_BASE_PATH} does not exist"
  exit 1
fi

if ! [[ -d ${TEMPLATE_INSTALL_BASE_PATH}/template ]];
then
  cd "${TEMPLATE_INSTALL_BASE_PATH}" || exit 1

  if [[ -n "$GIT_REPO_URL" ]]; then
    echo "cloning ${GIT_REPO_URL}"
    git clone -b "${GIT_BRANCH}" "${GIT_REPO_URL}" piperci-faas-templates
    mv piperci-faas-templates/template ./template
    rm -rf piperci-faas-templates
  else
    curl -o template.tar.gz "${CURL_DOWNLOAD_URL}"
    tar -xzf template.tar.gz --strip-components=1
  fi
else
  echo "template already installed for this env skipping"
fi
